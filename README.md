# Back-end App Test

Back-end application development task. Back-end app should be developed in express.js framework. Database can be used SQL / NoSQL (Postgress, MySQL, MongoDB, InfluxDB, etc). Coding should be done using "camelCase" standard. Each function & business logic should have proper commenting.

**NOTE:** 
- Don't push the code in the master branch. Fork the branch and then start developing the codebase.
- Don't add node_modules directory
- create "backend" directory for Back-End codebase
- create "database" directory for datatabase file
- Nice to have feature - Docker & docker-compose.yml file [ Build and generate artifacts ]

**Problem Statement:**
Design and develop a back-end application which should return an Restful API for employee management.

**API List:**
1. `/login` API that checks if the username && password is correct
2. `/getEmployee/:e_id` API that provides detail of given employee_id
3. `/getEmployees` API that provides list of all employees

**User guide**
1. Create a .env file from the .sample-env and fill in the correct information for the database you are connecting to
2. enter "npm start" in the terminal

**Additional comments**
I chose to type out the sql queries rather than use an ORM such as sequelize for this as I am unsure of how resync manage its backend.
Thus I feel SQL would be the safest and will at least be relevant. The password is also hashed before the has is stored in the database.