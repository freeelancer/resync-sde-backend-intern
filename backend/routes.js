var express = require('express');
var router = express.Router();
var sql = require("../database/sql");
const dbQuery = require("./query");
const bcrypt = require('bcrypt');

var { errorMessage, successMessage, status } = require("./helpers/status");

router.get('/', async (req, res) => {
    return res.send("resync-sde-backend-intern")
})

router.get('/getEmployees', async (req, res) => {
    const { rows } = await dbQuery.query(sql.query.get_all_employees);
    const result = await rows;
    return res.send(result);
});

router.get('/getEmployee/:employeeId', async (req, res) => {
    const { rows } = await dbQuery.query(sql.query.select_employee_by_id, [req.params.employeeId]);
    const result = await rows[0];
    return res.send(result);
});

router.post('/login', async (req, res) => {
    const { username, password } = req.body
    const { rows } = await dbQuery.query(sql.query.select_employee_by_username, [username]);
    const result = await rows[0];
    if (result == undefined) {
        errorMessage.error = "Username not found";
        return res.status(status.error).send(errorMessage);
    }
    bcrypt.compare(password, result.password_hashed).then(result => {
        if (result) {
            return res.send("Password is correct")
        } else {
            return res.send("Password is wrong")
        }
    });
});


module.exports = router;