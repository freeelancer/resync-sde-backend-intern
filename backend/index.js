const express = require('express');
const app = express();
const initialize = require("./initialize")
var port = normalizePort(process.env.PORT || '3000');
var indexRouter = require('./routes.js');
var cors = require("cors");

function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 0) {
        // port number
        return port;
    }
    return false;
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

//Routes//
app.use('/', indexRouter);


app.listen(port, () => {
    console.log("listening on port ", port)
})

//Initialize data by removing employees table if it exists and creating the employees table before populating it with one test data
initialize();