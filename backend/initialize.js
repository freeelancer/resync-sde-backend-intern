const dbQuery = require("./query");
var sql = require("../database/sql");
const bcrypt = require('bcrypt');
const saltRounds = 10;


module.exports = async () => {
    await dbQuery.query(sql.query.drop_all_tables);
    await dbQuery.query(sql.query.table_creation);
    const myPlaintextPassword = 'password';
    let hash = await bcrypt.hash(myPlaintextPassword, saltRounds)
    let values = await ["tester", "tester", hash, "user123"]
    await dbQuery.query(sql.query.insert_employee, values)
}