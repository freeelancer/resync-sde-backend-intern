DROP TABLE IF EXITS employees

CREATE TABLE employees(
    employeeId SERIAL PRIMARY KEY,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    password_hashed VARCHAR(255),
    username VARCHAR(255) UNIQUE
);