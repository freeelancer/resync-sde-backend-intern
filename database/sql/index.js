const sql = {}

//Collection of all the SQL queries used
sql.query = {
    drop_all_tables: "DROP TABLE IF EXISTS employees",
    table_creation: "CREATE TABLE employees(\
        employeeId SERIAL PRIMARY KEY,\
        firstName VARCHAR(255),\
        lastName VARCHAR(255),\
        password_hashed VARCHAR(255),\
        username VARCHAR(255) UNIQUE\
    );",
    insert_employee: "INSERT INTO employees(firstName, lastName, password_hashed, username) VALUES($1, $2, $3, $4) ON CONFLICT DO NOTHING",
    select_employee_by_username: "SELECT * FROM employees e WHERE e.username=$1",
    select_employee_by_id: "SELECT * FROM employees e WHERE e.employeeId=$1",
    get_all_employees: "SELECT * FROM employees"
}


module.exports = sql